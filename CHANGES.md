Latest changes
===============

Release 0.5
------------

      * New Features

         - `bx qsmxt`
         - `bx dartel`
         - `bx tau`

      * Improvements

         - Switch testing framework to `pytest`
         - Add `URL` attribute to all commands (pipeline documentation) 
         - Expose `dump` as a callable bin script
         - Support Python 3.8 for CI 
         
      * Fixes

        - Use up-to-date ALFA list in `dump`
        - Updated/curated PET, DARTEL, LONG, VBM, ALFA lists

Release 0.4
------------

      * New Features

         - FreeSurfer7 extra segmentation modules (hypothalamus, brainstem, ...)
         - bx fdg/ftm mri_ids
         - `bx bamos snapshot`
         - `bx bamos report`

      * Improvements

         - Updates on ALFA lists
         - New lists for PENSA
         
      * Fixes
        - Dickerson signatures now uses properly coregistered meshes/thickness


Release 0.3
------------

      * New Features

         - `bx freesurfer7`
         - `bx donsurf`
         - `bx bamos snapshot`
         - `bx bamos report`

      * Improvements

         - `mrdates` renamed to `scandates`
         - `bx nifti` now accepts wildcards


Release 0.2
-------------

      * New Features

         - `bx braak`
         - `bx bamos`
         - `bx ftm`
         - `bx fdg`
         - `bx signature`
         - `bx dtifit maps`
         - A script (`tests/update_readme.py`) to help update the README
         - Added two lists for PET images

      * Improvements

         - Reduced startup time
         - Rich-formatted documentation

      * Bug fixes

         - Removing some incorrect Freesurfer6 HIRES volumes (see FS website)

Release 0.1.3
---------------

      * New Features

         - `bx archiving tests`

      * Improvements

         - Collecting validation tests (now possible for various versions)

      * Bug fixes

         - Collecting tests can be interrupted (and partial result will be saved)

Release 0.1.2
---------------

      * New Features

         - Wrong (sub)commands return informative message

      * Improvements

         - Docstrings and help messages         

      * Bug fixes

         - Improved tests
         - Default server URL if missing .xnat.cfg
